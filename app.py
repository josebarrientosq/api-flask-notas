import io
import os
import json
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient
from bson import json_util

#from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp

from functools import wraps
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token,
                                get_jwt_identity,  get_jwt_claims, verify_jwt_in_request)


app = Flask(__name__)
app.debug = True
app.config['JWT_SECRET_KEY'] = 'B1Admin'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False

jwt = JWTManager(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#client = MongoClient('localhost:27017')
db = client.tododb

s = db.user.find({'username': 'admin'}).count()
if s == 0:
    db.user.insert(
        {'username': 'admin', 'password': 'admin', 'roles': 'admin'})


@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    if username:
        user = db.user.find_one({'username': username})
    else:
        return jsonify({"msg": "Missing username parameter"}), 400

    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    if user and safe_str_cmp(user["password"].encode('utf-8'), password.encode('utf-8')):
        # Identity can be any data that is json serializable
        access_token = create_access_token(identity=user)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({"msg": "Bad username or password"}), 401

# chapter 5


@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'roles': user["roles"]}


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user["username"]

# chapter 7


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['roles'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper


@app.route('/')
# @admin_required
def todo():

    _items = db.user.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)


@app.route('/get-one-user', methods=['POST'])
@admin_required
def get_one_user():
    username = request.json['username']
    s = db.user.find_one({'username': username})
    if s:
        output = {"success": True,
                  "username": s["username"],
                  "password": s["password"],
                  "roles": s["roles"],
                  'transacciones': s['transacciones']
                  }
    else:
        output = {"success": False}

    return jsonify(output)


@app.route('/get-all-user', methods=['POST'])
@admin_required
def get_all_user():
    todos = db.user.find({})
    documents = [todo for todo in todos]
    return json_util.dumps(documents)
    # return jsonify(output)


@app.route('/add-one-user', methods=['POST'])
@admin_required
def add_one_user():
    username = request.json['username']
    password = request.json['password']
    roles = request.json['roles']

    s = db.user.find({'username': username}).count()

    if s == 0:
        db.user.insert({'username': username, 'password': password,
                       'roles': roles, 'transacciones': 0})
        output = {"success": True,
                  "username": username,
                  "password": password,
                  "roles": roles,
                  "msg": "Se inserto 1 usuario"

                  }
    else:
        output = {"success": False,
                  "msg": "ya existen "+str(s)+" registro"}

    return jsonify(output)


@app.route('/agregar-nota', methods=['POST'])
@jwt_required
def agregarnota():
    nota = request.json['nota']
    current_user = get_jwt_identity()

    s = db.user.update({'username': current_user}, {
        "$push": {"notas": [{
            "nombre": nota
        }]}
    })

    if s['nModified'] == 0:
        output = {"success": False,
                  "msg": "no se agrego la nota",
                  }
    else:
        output = {"success": True,
                  "msg": "Se agrego la nota",
                  }

    return jsonify(output), 200


@app.route('/borrar-nota', methods=['POST'])
@jwt_required
def borrarnota():
    nota = request.json['nota']
    current_user = get_jwt_identity()

    s = db.user.update({'username': current_user}, {
        "$pull": {"notas": [{
            "nombre": nota
        }]}
    })
    if s['nModified'] == 0:
        output = {"success": False,
                  "msg": "no se encontro la nota",
                  }
    else:
        output = {"success": True,
                  "msg": "Se borro la nota",
                  }

    return jsonify(output), 200


@app.route('/delete-all-user', methods=['POST'])
@admin_required
def deleteallusers():
    db.user.remove({})
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200


@app.route('/delete-one-user', methods=['POST'])
@admin_required
def deleteoneuser():
    username = request.json['username']
    s = db.user.delete_one({'username': username})
    if s.deleted_count == 1:
        output = {"success": True,
                  "data": s.deleted_count,
                  "msg": "Se borro el usuario " + username
                  }
    else:
        output = {"success": False,
                  "msg": "Se borraron " + str(s.deleted_count)+" registros"}

    return jsonify(output)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
